# Problem 7 (10001st prime)

# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
#   we can see that the 6th prime is 13.

# What is the 10 001st prime number?


# The magic numbers here are a cheap hack... I basically did binary search
#  until I was in the ballpark of the 10,001st prime... also, still slow;
#  this takes about 5s on my laptop to execute.
# TODO: Revisit code for prime generation; come up with a better way
#  to generate just the number of primes necessary
require_relative 'helpers/primes'
primes = Primes.primes(110_000)
puts primes[10_000]
