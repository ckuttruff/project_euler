#include <stdio.h>
#include <math.h>
#define DESIRED_FACTORS 501

int num_factors(int n) {
  int i;
  int count    = 2;  // includes 1 and n
  double sqrt_n = sqrt(n);

  for(i = 2; i <= sqrt_n; ++i)
    if(n % i == 0)
      count = count + 2;
  return count;
}

int main(void) {
  int i = 1;
  int tri_num = 1;

  while(num_factors(tri_num) < DESIRED_FACTORS) {
    ++i;
    tri_num = tri_num + i;
  }

  printf("%d\n", tri_num);

  return 0;
}
