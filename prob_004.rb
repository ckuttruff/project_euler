# Problem 4 (Largest palindrome product)

# A palindromic number reads the same both ways. The largest palindrome
#   made from the product of two 2-digit numbers is 9009 = 91 x 99.
# Find the largest palindrome made from the product of two 3-digit numbers.

MAX_NUM  = 999
MAX_PROD = MAX_NUM ** 2
# maximum number of digits
MAX_DIGS = MAX_NUM.to_s.length

# Check if palindrome is divisible by two numbers of MAX_DIGS length
def passes_division_test?(n)
  factors = MAX_NUM.downto(10**(MAX_DIGS - 1)).lazy
  factors.detect { |f| ((n % f == 0) &&
                        (n / f).to_s.length == MAX_DIGS) }
end

def palindrome?(n)
  n_str   = n.to_s
  mid     = n_str.length / 2
  rhalf_i = n_str.length.odd? ? (mid + 1) : mid

  lh, rh = n_str[0...mid], n_str[rhalf_i..-1]
  lh == rh.reverse
end

def max_palindrome(max_num)
  nums = max_num.downto(1).lazy
  nums.detect{ |n| palindrome?(n) && passes_division_test?(n) }
end

puts max_palindrome(MAX_PROD)
